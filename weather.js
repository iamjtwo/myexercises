function mygen() {
    fetch("http://api.openweathermap.org/data/2.5/weather?id=1706801&APPID=47301692bcd1e16cff4f98dd942b32fc").then(function(res){
        return res.json();
    }).then(function(data){

      console.log(data);
      var icon = 'http://openweathermap.org/img/w/' + data.weather[0].icon + '.png';
      document.getElementById('name').innerHTML = data.name+", "+data.sys.country;
      document.getElementById('weather').innerHTML =  "<img src='" + icon + "'><br>" + data.weather[0].description ;
      document.getElementById('coord').innerHTML = "["+ data.coord.lat +", "+ data.coord.lon +"]";
      document.getElementById('winddeg').innerHTML =  data.wind.deg;
      document.getElementById('windspeed').innerHTML = data.wind.speed+" m/s<br>" + data.main.pressure +" hpa";
      document.getElementById('humid').innerHTML = data.main.humidity +" %";
      document.getElementById('sunrise').innerHTML = new Date(data.sys.sunrise * 1000);
      document.getElementById('sunset').innerHTML = new Date(data.sys.sunset * 1000);

      });
}
mygen();

function myweath() {
    fetch("http://api.openweathermap.org/data/2.5/weather?id=1714264&APPID=47301692bcd1e16cff4f98dd942b32fc").then(function(res){
        return res.json();
    }).then(function(data){

      var icon = 'http://openweathermap.org/img/w/' + data.weather[0].icon + '.png';
      document.getElementById('name1').innerHTML = data.name+", "+data.sys.country;
      document.getElementById('weather1').innerHTML =  "<img src='" + icon + "'><br>" + data.weather[0].description ;
      document.getElementById('coord1').innerHTML = "["+ data.coord.lat +", "+ data.coord.lon +"]";
      document.getElementById('winddeg1').innerHTML =  data.wind.deg;
      document.getElementById('windspeed1').innerHTML = data.wind.speed+" m/s<br>" + data.main.pressure +" hpa";
      document.getElementById('humid1').innerHTML = data.main.humidity +" %";
      document.getElementById('sunrise1').innerHTML = new Date(data.sys.sunrise * 1000);
      document.getElementById('sunset1').innerHTML = new Date(data.sys.sunset * 1000);

      });
}
myweath();
